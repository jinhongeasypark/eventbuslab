//
//  Tracker.swift
//  eventbuslab
//
//  Created by Jin Hong on 2019-08-05.
//  Copyright © 2019 junk. All rights reserved.
//

protocol Tracker: EventSubscriber {
    func setup(eb: EventBus)
    func tearDown(eb: EventBus)
}

class MixpanelTracker: Tracker {
    init() {

    }

    func setup(eb: EventBus) {
        eb.subscribe(self, to: DidLoadEvent.self, onEvent: didLoad)
        eb.subscribe(self, to: DidAppearEvent.self, onEvent: didAppear)
    }

    func tearDown(eb: EventBus) {
        eb.unsubscribeFromAllEvents(self)
    }

    private func didLoad(event: DidLoadEvent) {
        print("MIXPANEL DID LOAD")
    }

    private func didAppear(event: DidAppearEvent) {
        print("MIXPANEL DID APPEAR")
    }
}

class OtherTracker: Tracker {
    init() {

    }

    func setup(eb: EventBus) {
        eb.subscribe(self, to: DidLoadEvent.self, onEvent: didLoad)
        eb.subscribe(self, to: DidAppearEvent.self, onEvent: didAppear)
    }

    func tearDown(eb: EventBus) {
        eb.unsubscribeFromAllEvents(self)
    }

    private func didLoad(event: DidLoadEvent) {
        print("OTHER DID LOAD")
    }

    private func didAppear(event: DidAppearEvent) {
        print("OTHER DID APPEAR")
    }
}
