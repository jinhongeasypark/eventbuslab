//
//  Junk.swift
//  eventbuslab
//
//  Created by Jin Hong on 2019-08-05.
//  Copyright © 2019 junk. All rights reserved.
//

import UIKit

class AViewController: UIViewController {

    let g: EventBus = EventBusSingleton.shared
    let h: EventBus = MapTableEventBus.shared

    let ot: Tracker = OtherTracker()
    let mt: Tracker = MixpanelTracker()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .cyan

        ot.setup(eb: g)
//        ot.setup(eb: h)
//        mt.setup(eb: g)
        mt.setup(eb: h)

        let loadButton = UIButton(type: .system)
        loadButton.setTitle("LOAD", for: .normal)
        loadButton.addTarget(self, action: #selector(load), for: .touchUpInside)
        loadButton.frame = .init(x: 50, y: 200, width: 200, height: 200)
        view.addSubview(loadButton)

        let appearButton = UIButton(type: .system)
        appearButton.setTitle("APPEAR", for: .normal)
        appearButton.addTarget(self, action: #selector(appear), for: .touchUpInside)
        appearButton.frame = .init(x: 50, y: 500, width: 200, height: 200)
        view.addSubview(appearButton)

        print("DID LOAD")
        let e = DidLoadEvent(id: "DL", description: "DID LOAD YO")
        g.publish(e)
        h.publish(e)
        print("DID LOAD PUBD")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("DID APPEAR")
        let e = DidAppearEvent(id: "DA", description: "DID APPEAR YO")
        g.publish(e)
        h.publish(e)
        print("DID APPEAR PUBD")
    }

    @objc private func load(_ sender: UIButton) {
        let e = DidLoadEvent(id: "DL", description: "DID LOAD YO")
//        g.publish(e)
        h.publish(e)
        ot.tearDown(eb: g)
    }

    @objc private func appear(_ sender: UIButton) {
        let e = DidAppearEvent(id: "DA", description: "DID APPEAR YO")
        g.publish(e)
        mt.tearDown(eb: h)
    }
}
