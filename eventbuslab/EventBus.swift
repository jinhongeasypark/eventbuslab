//
//  EventBus.swift
//  eventbuslab
//
//  Created by Jin Hong on 2019-08-05.
//  Copyright © 2019 junk. All rights reserved.
//

import Foundation

fileprivate class Subscription {
    let subscriber: EventSubscriber
    let onEvent: (Any) -> Void
    init(subscriber: EventSubscriber, onEvent: @escaping (Any) -> Void) {
        self.subscriber = subscriber
        self.onEvent = onEvent
    }
}

fileprivate class SubscriptionArray {
    var subscriptions: [Subscription]
    init(subscriptions: [Subscription]) {
        self.subscriptions = subscriptions
    }
}

protocol EventBus: EventPublisher {
    func subscribe<T: Event>(_ subscriber: EventSubscriber, to Event: T.Type, onEvent: @escaping (T) -> Void)
    func unsubscribe<T: Event>(_ subscriber: EventSubscriber, from Event: T.Type)
    func unsubscribeFromAllEvents(_ subscriber: EventSubscriber)
}

class MapTableEventBus: EventBus {
    private typealias EventName = NSString
    static let shared: EventBus = MapTableEventBus()
    private var subscriptionMap: NSMapTable<EventName, SubscriptionArray>

    private init() {
        subscriptionMap = .init(keyOptions: .strongMemory, valueOptions: .strongMemory)
    }

    func publish<T>(_ event: T) where T : Event {
        let eventName = T.name as EventName
        subscriptionMap.object(forKey: eventName).map { subscriptionArray in
            subscriptionArray.subscriptions.forEach { subscription in
                subscription.onEvent(event)
            }
        }
    }

    func subscribe<T>(_ subscriber: EventSubscriber, to Event: T.Type, onEvent: @escaping (T) -> Void) where T : Event {
        let eventName = Event.name as EventName
        let existingSubscriptions = subscriptionMap.object(forKey: eventName)
        let subscription = Subscription(subscriber: subscriber) { [onEvent] event in
            guard let concreteEvent = event as? T else { return }
            onEvent(concreteEvent)
        }
        existingSubscriptions?.subscriptions.append(subscription)
        let ss = existingSubscriptions ?? SubscriptionArray(subscriptions: [subscription])
        subscriptionMap.setObject(ss, forKey: eventName)
    }

    func unsubscribe<T: Event>(_ subscriber: EventSubscriber, from Event: T.Type) {
        let eventName = Event.name as EventName
        guard let existingSubscription = subscriptionMap.object(forKey: eventName) else { return }
        let updatedSubscriptions = existingSubscription.subscriptions.filter { subscription -> Bool in
            return subscription.subscriber !== subscriber
        }
        let newSubscriptions = updatedSubscriptions.isEmpty ? nil : SubscriptionArray(subscriptions: updatedSubscriptions)
        updatedSubscriptions.isEmpty ?
            subscriptionMap.removeObject(forKey: eventName) :
            subscriptionMap.setObject(newSubscriptions, forKey: eventName)
    }

    func unsubscribeFromAllEvents(_ subscriber: EventSubscriber) {
        var updatedSubscriptionMap: NSMapTable<EventName, SubscriptionArray> = .init(keyOptions: .strongMemory, valueOptions: .strongMemory)
        defer { subscriptionMap = updatedSubscriptionMap }
        let keyEnumerator = subscriptionMap.keyEnumerator()
        while let key = keyEnumerator.nextObject() as? EventName {
            subscriptionMap.object(forKey: key).map { subscriptionArray in
                let updatedSubscriptions = subscriptionArray.subscriptions.filter { $0.subscriber !== subscriber }
                let newSubscriptionArray = updatedSubscriptions.isEmpty ? nil : SubscriptionArray(subscriptions: updatedSubscriptions)
                updatedSubscriptions.isEmpty ?
                    updatedSubscriptionMap.removeObject(forKey: key) :
                    updatedSubscriptionMap.setObject(newSubscriptionArray, forKey: key)
            }
        }
    }
}

class EventBusSingleton: EventBus {

    private typealias EventName = String
    static let shared: EventBus = EventBusSingleton()
    private var subscriptions: [EventName: [Subscription]]

    private init() {
        subscriptions = [:]
    }

    func publish<T: Event>(_ event: T) {
        let eventName = T.name
        subscriptions[eventName]?.forEach { subscription in
            subscription.onEvent(event)
        }
    }

    func subscribe<T: Event>(_ subscriber: EventSubscriber, to Event: T.Type, onEvent: @escaping (T) -> Void) {
        let eventName = Event.name
        let existingSubscriptions = subscriptions[eventName]
        let subscription = Subscription(subscriber: subscriber) { [onEvent] event in
            guard let concreteEvent = event as? T else { return }
            onEvent(concreteEvent)
        }
        subscriptions[eventName] = existingSubscriptions.map { $0 + [subscription] } ?? [subscription]
    }

    func unsubscribe<T: Event>(_ subscriber: EventSubscriber, from Event: T.Type) {
        let eventName = Event.name
        guard var existingSubscription = subscriptions[eventName] else { return }
        existingSubscription.removeAll { subscription -> Bool in
            return subscription.subscriber === subscriber
        }
        subscriptions[eventName] = existingSubscription.isEmpty ? nil : existingSubscription
    }

    func unsubscribeFromAllEvents(_ subscriber: EventSubscriber) {
        subscriptions.forEach { (eventName, subscriptions) in
            let filteredSubscriptions = subscriptions.filter { $0.subscriber !== subscriber }
            let newSubscriptions = filteredSubscriptions.isEmpty ? nil : filteredSubscriptions
            self.subscriptions[eventName] = newSubscriptions
        }
    }
}
