//
//  Event.swift
//  eventbuslab
//
//  Created by Jin Hong on 2019-08-05.
//  Copyright © 2019 junk. All rights reserved.
//

protocol Event {

}
extension Event {
    static var name: String { return String(describing: self) }
}

struct DidLoadEvent: Event {
    let id: String
    let description: String
}

struct DidAppearEvent: Event {
    let id: String
    let description: String
}

protocol EventSubscriber: class {

}

protocol EventPublisher {
    func publish<T: Event>(_ event: T)
}
